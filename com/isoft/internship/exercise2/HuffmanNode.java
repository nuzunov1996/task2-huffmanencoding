package com.isoft.internship.exercise2;

public class HuffmanNode implements Comparable<HuffmanNode>
{
    public int frequency;
    public char data;
    public HuffmanNode left, right;

    public HuffmanNode(char data, int frequency, HuffmanNode left, HuffmanNode right)
    {
        this.frequency = frequency;
        this.data = data;
        this.left = left;
        this.right = right;
    }

    @Override
    public int compareTo(HuffmanNode node)
    {
        return this.frequency - node.frequency;
    }
}
