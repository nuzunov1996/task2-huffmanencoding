package com.isoft.internship.exercise2;

import java.util.HashMap;
import java.util.Map;

public class HuffmanEncoding
{
    private static int encodedTextLength = 0;
    private static int huffmanTreeHeight = 0;
    private static Map<Character, String>prefixMap = new HashMap<>();

    public static int getEncodedTextLength()
    {
        return encodedTextLength;
    }

    public static int getHuffmanTreeHeight()
    {
        return huffmanTreeHeight;
    }

    public static void encode(String textForEncoding)
    {
        Map<Character, Integer> frequency = getSymbolsAndTheirFrequency(textForEncoding);

        //build huffman tree
        HuffmanTree huffmanTree = new HuffmanTree();
        HuffmanNode root = huffmanTree.buildTree(frequency);
        setPrefixCodes(root, new StringBuilder());

        calculateEncodedTextLength(frequency);
        huffmanTreeHeight = huffmanTree.maxDepth(root);
    }

    private static void setPrefixCodes(HuffmanNode node, StringBuilder prefix)
    {
        if (node != null)
        {
            if (node.left == null && node.right == null)
            {
                prefixMap.put(node.data, prefix.toString());
            }
            else
            {
                prefix.append('0');
                setPrefixCodes(node.left, prefix);
                prefix.deleteCharAt(prefix.length() - 1);

                prefix.append('1');
                setPrefixCodes(node.right, prefix);
                prefix.deleteCharAt(prefix.length() - 1);
            }
        }
    }

    private static Map<Character, Integer> getSymbolsAndTheirFrequency(String textForEncoding)
    {
        Map<Character, Integer> frequency = new HashMap<>();
        for (int i = 0; i < textForEncoding.length(); i++)
        {
            frequency.merge(textForEncoding.charAt(i),1, Integer::sum);
        }

        return frequency;
    }

    private static void calculateEncodedTextLength(Map<Character, Integer> frequency)
    {
        for (Character c : frequency.keySet())
        {
            if (prefixMap.containsKey(c))
            {
                encodedTextLength = encodedTextLength + (prefixMap.get(c).length() * frequency.get(c));
            }
        }
    }
}
