package com.isoft.internship.exercise2;

import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

public class HuffmanTree
{
    public HuffmanNode buildTree(Map<Character, Integer> frequency)
    {
        PriorityQueue<HuffmanNode> priorityQueue = new PriorityQueue<>();
        if (frequency.isEmpty())
        {
            return null;
        }

        HuffmanNode node;
        for (Character c : frequency.keySet())
        {
            node = new HuffmanNode(c, frequency.get(c), null, null);
            priorityQueue.offer(node);
        }
        while (priorityQueue.size() > 1)
        {
            HuffmanNode x = priorityQueue.poll();
            HuffmanNode y = priorityQueue.poll();
            int sumFrequency = x.frequency + y.frequency;
            HuffmanNode sum = new HuffmanNode('-', sumFrequency, x, y);
            priorityQueue.offer(sum);
        }
        HuffmanNode root = priorityQueue.poll();

        return root;
    }

    public int maxDepth(HuffmanNode node)
    {
        if (node == null)
        {
            return 0;
        }
        else
        {
            /* compute the depth of each subtree */
            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);

            /* use the larger one */
            if (lDepth > rDepth)
            {
                return (lDepth + 1);
            }
            else
            {
                return (rDepth + 1);
            }
        }
    }

}
